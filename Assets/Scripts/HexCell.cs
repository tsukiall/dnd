﻿using UnityEngine;

public class HexCell {
    private Vector3 gridPosition;
    public Vector3 localPosition;
    public Vector3 worldPosition;

    public Vector3 hexExt;
    public Vector3 hexCenter;

    public HexChunk parentChunk;

    public Mesh localMesh;
    public int terrainID;

    public Vector3[] vertices;
    public Vector2[] uv;
    public int[] triangles;
    
    public Vector2 AxialGridPosition {
        get { return new Vector2(CubeGridPosition.x, CubeGridPosition.y); }
    }
    public Vector3 CubeGridPosition {
        get { return gridPosition; }
        set { gridPosition = value; }
    }

    public void Start() {
        MeshSetup();
    }

    void MeshSetup() {
        localMesh = new Mesh();

        localMesh.vertices = parentChunk.worldManager.hexSharedMesh.vertices;
        localMesh.triangles = parentChunk.worldManager.hexSharedMesh.triangles;
        Vector2[] hexUv = parentChunk.worldManager.hexSharedMesh.uv;
        for(int i = 0; i < hexUv.Length; i++) {
            hexUv[i].x += 0.1f * terrainID;
        }
        localMesh.uv = hexUv;

        localMesh.RecalculateNormals();
    }
}