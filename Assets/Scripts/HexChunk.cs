﻿using System.Collections.Generic;
using UnityEngine;

public class HexChunk : MonoBehaviour {

    [SerializeField]
    public HexCell[,] hexArray;
    public int size = 16;
    public Vector3 hexSize;

    public int xSector;
    public int ySector;
    public WorldController worldManager;

    MeshFilter filter;
    BoxCollider collider;

    public void OnDestroy() {
        Destroy(GetComponent<Renderer>().material);
    }
    
    public void AllocateHexArray() {
        hexArray = new HexCell[size, size];
    }

    public void Begin() {
        GenerateChunk();
        for(int x = 0; x < size; x++) {
            for(int z = 0; z < size; z++) {
                if(hexArray[x, z] != null) {
                    hexArray[x, z].parentChunk = this;
                    int xCoord = x + (xSector * 16);
                    int zCoord = z + (ySector * 16);
                    hexArray[x, z].terrainID = worldManager.hexMap[xCoord, zCoord];
                    hexArray[x, z].Start();
                } else {
                    print("null hexagon in memory");
                }
            }
        }
        Combine();
    }

    public void SetSize(int x) {
        size = x;
    }

    public void GenerateChunk() {
        bool odd;
        for(int z = 0; z < size; z++) {
            odd = (z % 2) == 0;
            if(odd) {
                for(int x = 0; x < size; x++) {
                    GenerateHex(x, z);
                }
            } else {
                for(int x = 0; x < size; x++) {
                    GenerateHexOffset(x, z);
                }
            }
        }
    }

    public void GenerateHex(int x, int z) {
        HexCell hex;
        Vector2 worldArrayPosition;
        hexArray[x, z] = new HexCell();
        hex = hexArray[x, z];

        worldArrayPosition.x = x + (size * xSector);
        worldArrayPosition.y = z + (size * ySector);

        hex.CubeGridPosition = new Vector3(worldArrayPosition.x - Mathf.Round((worldArrayPosition.y / 2) + .1f), worldArrayPosition.y, -(worldArrayPosition.x - Mathf.Round((worldArrayPosition.y / 2) + .1f) + worldArrayPosition.y));
        hex.localPosition = new Vector3((x * (worldManager.hexExt.x * 2) + worldManager.hexExt.x), 0, (z * worldManager.hexExt.z) * 1.5f);
        hex.worldPosition = new Vector3(hex.localPosition.x + (xSector * (size * hexSize.x)), hex.localPosition.y, hex.localPosition.z + ((ySector * (size * hexSize.z)) * (.75f)));
        
        hex.hexExt = worldManager.hexExt;
        hex.hexCenter = worldManager.hexCenter;
    }

    public void GenerateHexOffset(int x, int z) {
        HexCell hex;
        Vector2 worldArrayPosition;
        hexArray[x, z] = new HexCell();
        hex = hexArray[x, z];

        worldArrayPosition.x = x + (size * xSector);
        worldArrayPosition.y = z + (size * ySector);

        hex.CubeGridPosition = new Vector3(worldArrayPosition.x - Mathf.Round((worldArrayPosition.y / 2) + .1f), worldArrayPosition.y, -(worldArrayPosition.x - Mathf.Round((worldArrayPosition.y / 2) + .1f) + worldArrayPosition.y));
        hex.localPosition = new Vector3((x * (worldManager.hexExt.x * 2) + worldManager.hexExt.x * 2), 0, (z * worldManager.hexExt.z) * 1.5f);
        hex.worldPosition = new Vector3(hex.localPosition.x + (xSector * (size * hexSize.x)), hex.localPosition.y, hex.localPosition.z + ((ySector * (size * hexSize.z)) * (.75f)));

        hex.hexExt = worldManager.hexExt;
        hex.hexCenter = worldManager.hexCenter;
    }

    private void Combine() {
        CombineInstance[,] combine = new CombineInstance[size, size];
        for(int x = 0; x < size; x++) {
            for(int z = 0; z < size; z++) {
                combine[x, z].mesh = hexArray[x, z].localMesh;
                Matrix4x4 matrix = new Matrix4x4();
                matrix.SetTRS(hexArray[x, z].localPosition, Quaternion.identity, Vector3.one);
                combine[x, z].transform = matrix;
            }
        }
        filter = gameObject.GetComponent<MeshFilter>();
        filter.mesh = new Mesh();
        CombineInstance[] final;
        ToSingleArray(combine, out final);
        filter.mesh.CombineMeshes(final);
        filter.mesh.RecalculateNormals();
        filter.mesh.RecalculateBounds();
        MakeCollider();
    }

    void MakeCollider() {
        if(collider == null) {
            collider = gameObject.AddComponent<BoxCollider>();
        }
        collider.center = filter.mesh.bounds.center;
        collider.size = filter.mesh.bounds.size;
    }

    public static void ToSingleArray(CombineInstance[,] doubleArray, out CombineInstance[] singleArray) {
        List<CombineInstance> combineList = new List<CombineInstance>();

        foreach(CombineInstance combine in doubleArray) {
            combineList.Add(combine);
        }

        singleArray = combineList.ToArray();
    }
}
