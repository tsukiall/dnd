﻿using UnityEngine;
using UnityEngine.UI;
using System.Diagnostics;

public class WorldEditor : MonoBehaviour {
    public Slider sizeSlider;
    public Text sizeText;
    public Button advance;
    public GameObject[] onBtns = new GameObject[7];
    public GameObject[] offBtns = new GameObject[7];
    public int terrainCount = 7;
    public int[] terrainToggle = new int[7];
    public Stopwatch stopwatch;
    public GameObject[] panels;
    public GameObject[] buildings;
    public int buildingBorder;
    public bool canBuild = true;

    float horizontalOffset = 4.33f;
    float horizontalSpacing = 8.66f;
    float verticalSpacing = 7.5f;

    WorldController worldController;
    int btnIndex = 0;
    bool isBuilding = false;
    GameObject building;

    void Start() {
        worldController = GameObject.FindGameObjectWithTag("WorldController").GetComponent<WorldController>();
        worldController.worldEditor = this;
        stopwatch = new Stopwatch();
    }

    void Update() {
        worldController.chunks = (int)sizeSlider.value;
        sizeText.text = sizeSlider.value.ToString() + " chunks";
        if(worldController.chunkHolder) {
            advance.interactable = true;
        } else {
            advance.interactable = false;
        }

        if(isBuilding) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Vector3 pos = new Vector3();
            if(!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) {
                if(Physics.Raycast(ray, out hit, 100.0f)) {
                    int hexV = Mathf.Clamp(Mathf.RoundToInt(hit.point.z / verticalSpacing), buildingBorder, worldController.chunks * worldController.chunkSize - (buildingBorder + 1));
                    pos.z = hexV * verticalSpacing;
                    int hexH = 0;
                    if(hexV % 2 == 0) {
                        hexH = Mathf.Clamp(Mathf.RoundToInt((hit.point.x / horizontalSpacing) - 0.5f), buildingBorder, worldController.chunks * worldController.chunkSize - (buildingBorder + 1));
                        pos.x = horizontalOffset + hexH * horizontalSpacing;
                    } else {
                        hexH = Mathf.Clamp(Mathf.RoundToInt(hit.point.x / horizontalSpacing) - 1, buildingBorder, worldController.chunks * worldController.chunkSize - (buildingBorder + 1));
                        pos.x = horizontalOffset * 2.0f + hexH * horizontalSpacing;
                    }
                    building.transform.position = pos;
                    if(Input.GetMouseButton(0) && canBuild) {
                        isBuilding = false;
                    }
                }
            }
            if(Input.GetMouseButton(1)) {
                isBuilding = false;
                Destroy(building);
            }
        }
    }

    void Toggle(int index, bool toggle) {
        if(toggle) {
            terrainCount--;
            terrainToggle[index] = 0;
        } else {
            terrainCount++;
            terrainToggle[index] = 1;
        }
    }

    public void SetIndex(int index) {
        btnIndex = index;
    }

    public void ToggleTerrain(bool toggle) {
        Toggle(btnIndex, toggle);
        onBtns[btnIndex].SetActive(!toggle);
        if(terrainCount < 2) {
            foreach(GameObject btn in onBtns) {
                btn.GetComponent<Button>().interactable = false;
            }
        } else {
            if(onBtns[0].activeSelf == false) {
                foreach(GameObject btn in onBtns) {
                    btn.GetComponent<Button>().interactable = true;
                }
            }
        }
        offBtns[btnIndex].SetActive(toggle);
    }

    public void SwitchPanel(int index) {
        if(isBuilding) {
            Destroy(building);
            isBuilding = false;
        }
        foreach(GameObject panel in panels) {
            panel.SetActive(false);
        }
        panels[index].SetActive(true);
    }

    public void GenerateMap() {
        stopwatch.Reset();
        stopwatch.Start();
        worldController.DrawMap();
        stopwatch.Stop();
        UnityEngine.Debug.Log(stopwatch.Elapsed);
    }

    public void Build(int index) {
        if(isBuilding) {
            Destroy(building);
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Vector3 initPos = new Vector3();
        if(Physics.Raycast(ray, out hit, 100.0f)) {
            initPos = hit.transform.position;
        }
        building = Instantiate(buildings[index], initPos, Quaternion.identity, worldController.chunkHolder.transform);
        building.tag = "Building";
        building.AddComponent<SphereCollider>();
        building.GetComponent<SphereCollider>().isTrigger = true;
        building.AddComponent<Rigidbody>();
        building.GetComponent<Rigidbody>().isKinematic = true;
        building.AddComponent<BuildingController>();
        building.GetComponent<BuildingController>().worldEditor = this;
        buildingBorder = index + 1;
        isBuilding = true;
    }
}
