﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    void Awake() {
        OpenPanel(0);
    }

    public void OpenPanel(int panelIndex) {
        foreach(Transform child in transform) {
            child.gameObject.SetActive(false);
        }
        Transform activeChild = transform.GetChild(panelIndex);
        activeChild.gameObject.SetActive(true);
    }

    public void LoadScene(int sceneIndex) {
        SceneManager.LoadScene(sceneIndex);
    }

    public void ExitGame() {
        Application.Quit();
    }
}
