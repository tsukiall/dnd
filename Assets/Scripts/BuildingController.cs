﻿using UnityEngine;

public class BuildingController : MonoBehaviour {
    public WorldEditor worldEditor;

    void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Building")) {
            worldEditor.canBuild = false;
        }
    }

    void OnTriggerExit(Collider other) {
        if(other.CompareTag("Building")) {
            worldEditor.canBuild = true;
        }
    }
}
