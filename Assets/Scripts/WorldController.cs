﻿using UnityEngine;
using AccidentalNoise;

public class WorldController : MonoBehaviour {
    public int chunks;
    public WorldEditor worldEditor;

    public int octaves;
    public float frequency;

    public Mesh hexSharedMesh;
    public float hexRadius;

    [HideInInspector]
    public Vector3 hexExt;
    [HideInInspector]
    public Vector3 hexSize;
    [HideInInspector]
    public Vector3 hexCenter;
    [HideInInspector]
    public GameObject chunkHolder;

    public ImplicitFractal heightMap;
    float heightStep = 0.0f;

    public HexChunk[,] hexChunks;

    public Vector2 mapSize;
    public int chunkSize;
    public Material material;

    public int[,] hexMap;

    void Awake() {
        DontDestroyOnLoad(gameObject);
    }
    
    void GetHexProperties() {
        GameObject instance = new GameObject("Bounds Set Up: Flat");
        instance.AddComponent<MeshFilter>();
        instance.AddComponent<MeshRenderer>();
        instance.AddComponent<MeshCollider>();
        instance.transform.position = Vector3.zero;
        instance.transform.rotation = Quaternion.identity;

        Vector3[] vertices;
        int[] triangles;
        Vector2[] uv;

        vertices = new Vector3[] {
            new Vector3((hexRadius * Mathf.Cos((float)(2 * Mathf.PI * (3 + 0.5) / 6))), 0, (hexRadius * Mathf.Sin((float)(2*Mathf.PI*(3 + 0.5)/6)))),
            new Vector3((hexRadius * Mathf.Cos((float)(2 * Mathf.PI * (2 + 0.5) / 6))), 0, (hexRadius * Mathf.Sin((float)(2*Mathf.PI*(2 + 0.5)/6)))),
            new Vector3((hexRadius * Mathf.Cos((float)(2 * Mathf.PI * (1 + 0.5) / 6))), 0, (hexRadius * Mathf.Sin((float)(2*Mathf.PI*(1 + 0.5)/6)))),
            new Vector3((hexRadius * Mathf.Cos((float)(2 * Mathf.PI * (0 + 0.5) / 6))), 0, (hexRadius * Mathf.Sin((float)(2*Mathf.PI*(0 + 0.5)/6)))),
            new Vector3((hexRadius * Mathf.Cos((float)(2 * Mathf.PI * (5 + 0.5) / 6))), 0, (hexRadius * Mathf.Sin((float)(2*Mathf.PI*(5 + 0.5)/6)))),
            new Vector3((hexRadius * Mathf.Cos((float)(2 * Mathf.PI * (4 + 0.5) / 6))), 0, (hexRadius * Mathf.Sin((float)(2*Mathf.PI*(4 + 0.5)/6))))
        };

        triangles = new int[] {
            1,5,0,
            1,4,5,
            1,2,4,
            2,3,4
        };

        uv = new Vector2[] {
            new Vector2(0, 0.25f),
            new Vector2(0, 0.75f),
            new Vector2(0.05f, 1),
            new Vector2(0.1f, 0.75f),
            new Vector2(0.1f, 0.25f),
            new Vector2(0.05f, 0)
        };

        hexSharedMesh = new Mesh();
        hexSharedMesh.vertices = vertices;
        hexSharedMesh.triangles = triangles;
        hexSharedMesh.uv = uv;
        instance.GetComponent<MeshFilter>().mesh = hexSharedMesh;
        instance.GetComponent<MeshFilter>().mesh.RecalculateNormals();
        instance.GetComponent<MeshCollider>().sharedMesh = hexSharedMesh;
        hexExt = new Vector3(instance.GetComponent<MeshCollider>().bounds.extents.x, instance.GetComponent<MeshCollider>().bounds.extents.y, instance.GetComponent<MeshCollider>().bounds.extents.z);
        hexSize = new Vector3(instance.GetComponent<MeshCollider>().bounds.size.x, instance.GetComponent<MeshCollider>().bounds.size.y, instance.GetComponent<MeshCollider>().bounds.size.z);
        hexCenter = new Vector3(instance.GetComponent<MeshCollider>().bounds.center.x, instance.GetComponent<MeshCollider>().bounds.center.y, instance.GetComponent<MeshCollider>().bounds.center.z);
        Destroy(instance);
    }

    int GetTerrainFromPerlin(float perlin) {
        int offset = -1;
        if(perlin <= heightStep * 1) {
            offset = FindTerrainIndex(1, offset);
            return offset;
        }else if(perlin <= heightStep * 2) {
            offset = FindTerrainIndex(2, offset);
            return offset;
        } else if(perlin <= heightStep * 3) {
            offset = FindTerrainIndex(3, offset);
            return offset;
        } else if(perlin <= heightStep * 4) {
            offset = FindTerrainIndex(4, offset);
            return offset;
        } else if(perlin <= heightStep * 5) {
            offset = FindTerrainIndex(5, offset);
            return offset;
        } else if(perlin <= heightStep * 6) {
            offset = FindTerrainIndex(6, offset);
            return offset;
        } else {
            offset = FindTerrainIndex(7, offset);
            return offset;
        }
    }

    int FindTerrainIndex(int iteration, int offset) {
        int output = offset;
        for(int i = 0; i < iteration; i++) {
            output = System.Array.IndexOf(worldEditor.terrainToggle, 1, output + 1);
        }
        return output;
    }

    void GenerateTerrain() {
        heightMap = new ImplicitFractal(FractalType.MULTI, BasisType.SIMPLEX, InterpolationType.QUINTIC, octaves, frequency, Random.Range(0, int.MaxValue));
        int width = chunks * chunkSize;
        hexMap = new int[width, width];
        for(int x = 0; x < width; x++) {
            for(int y = 0; y < width; y++) {
                float x1 = x / (float)width;
                float y1 = y / (float)width;
                float value = Mathf.Abs((float)heightMap.Get(x1, y1));
                hexMap[x, y] = GetTerrainFromPerlin(value);
            }
        }
    }

    void GenerateMap() {
        hexChunks = new HexChunk[chunks, chunks];
        for(int x = 0; x < chunks; x++) {
            for(int z = 0; z < chunks; z++) {
                hexChunks[x, z] = NewChunk(x, z);
                hexChunks[x, z].gameObject.transform.position = new Vector3(x * (chunkSize * hexSize.x), 0f, (z * (chunkSize * hexSize.z) * (.75f)));
                hexChunks[x, z].hexSize = hexSize;
                hexChunks[x, z].SetSize(chunkSize);
                hexChunks[x, z].xSector = x;
                hexChunks[x, z].ySector = z;
                hexChunks[x, z].worldManager = this;
            }
        }
        foreach(HexChunk chunk in hexChunks) {
            chunk.Begin();
        }
    }

    public HexChunk NewChunk(int x, int z) {
        if(x == 0 && z == 0) {
            chunkHolder = new GameObject("ChunkHolder");
        }
        GameObject chunkObj = new GameObject("Chunk[" + x.ToString() + ", " + z.ToString() + "]");
        chunkObj.AddComponent<HexChunk>();
        chunkObj.GetComponent<HexChunk>().AllocateHexArray();
        chunkObj.AddComponent<MeshRenderer>().material = material;
        chunkObj.AddComponent<MeshFilter>();
        chunkObj.transform.parent = chunkHolder.transform;
        
        return chunkObj.GetComponent<HexChunk>();
    }

    void SplitHeight() {
        heightStep = 1.0f / worldEditor.terrainCount;
    }

    public void DrawMap() {
        if(chunkHolder) {
            Destroy(chunkHolder.gameObject);
        }
        SplitHeight();
        GenerateTerrain();
        GetHexProperties();
        GenerateMap();
    }
}
