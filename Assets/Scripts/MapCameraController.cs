﻿using UnityEngine;

public class MapCameraController : MonoBehaviour {
    public float minDist;
    public float maxDist;
    public float maxPosX;
    public float maxPosZ;
    public float vPos;
    public float hPos;
    public float scrollSensitivity;
    public float moveSensitivity;

    WorldController worldController;

    Vector3 lastPosition;

    void Start() {
        worldController = GameObject.FindGameObjectWithTag("WorldController").GetComponent<WorldController>();
    }

    public void MoveCamera() {
        hPos = worldController.chunks * worldController.chunkSize * 3.75f;
        vPos = worldController.chunks * worldController.chunkSize * 4.33f;
        Camera.main.transform.position = new Vector3(vPos, 100.0f, hPos);

        maxDist = worldController.chunks * worldController.chunkSize * 3.75f;
        maxPosX = worldController.chunks * worldController.chunkSize * 8.66f;
        maxPosZ = worldController.chunks * worldController.chunkSize * 7.5f;
    }
 
    void Update() {
        float fov = Camera.main.orthographicSize;
        if(Input.GetKey(KeyCode.PageDown)) {
            fov += scrollSensitivity * Time.deltaTime;
        }
        if(Input.GetKey(KeyCode.PageUp)) {
            fov -= scrollSensitivity * Time.deltaTime;
        }
        fov += Input.GetAxis("Mouse ScrollWheel") * -scrollSensitivity;
        fov = Mathf.Clamp(fov, minDist, maxDist);
        Camera.main.orthographicSize = fov;

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float moveSpeed = moveSensitivity * fov;
        Camera.main.transform.Translate(new Vector3(horizontal * moveSpeed, vertical * moveSpeed, 0.0f));
        Vector3 pos = Camera.main.transform.position;
        pos.Set(Mathf.Clamp(pos.x, 0.0f, maxPosX), pos.y, Mathf.Clamp(pos.z, 0.0f, maxPosZ));
        Camera.main.transform.position = pos;
    }
}
