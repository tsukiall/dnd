﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
    public GameObject worldController;

    void Awake() {
        DontDestroyOnLoad(gameObject);
        GameObject existingWC = GameObject.FindGameObjectWithTag("WorldController");
        if(!existingWC) {
            GameObject newController = Instantiate(worldController, new Vector3(0.0f, 0.0f, 0.0f), Quaternion.identity);
            newController.name = "World Controller";
        }
    }
}
